from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from accounts.forms import LogIn, SignUp


def user_login(request):
    if request.method == "POST":
        form = LogIn(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user:
                login(request, user)
                return redirect("list_projects")
    else:
        form = LogIn()
    context = {
        "form": form,
    }
    return render(request, "account/login.html", context)


def user_logout(request):
    logout(request)
    return redirect(user_login)


def user_signup(request):
    if request.method == "POST":
        form = SignUp(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            pass_confirm = form.cleaned_data["password_confirmation"]
            if password == pass_confirm:
                user = User.objects.create_user(
                    username=username,
                    password=password,
                )
                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error("password", "the passwords do not match")
    else:
        form = SignUp()
    context = {
        "form": form,
    }
    return render(request, "account/signup.html", context)
